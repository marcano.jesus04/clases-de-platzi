import { BrowserRouter, Route, Routes } from "react-router-dom"

import HomePage from "./pages/HomePage"
import AboutPage from "./pages/AboutPage"
import UsersPage from "./pages/UsersPage"
import NotFound from "./pages/Notfound"

export default function app() {
    return (
        <BrowserRouter>
            <Routes>
            <Route path='/' element={ <HomePage />}/>
            <Route path='/about' element={ <AboutPage />} />
            <Route path='/users' element={<UsersPage/>} />
            <Route path='*' element={<NotFound />} />
            </Routes>
        </BrowserRouter>
    )
}