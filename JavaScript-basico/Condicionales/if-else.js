var edad = 18;

if (edad === 18) {
    console.log("Hey, puedes votar. Será tu primera votación!")
} else if (edad > 18) {
    console.log("Puedes votar de nuevo")
} else {
    console.log("Aún no puedes votar")
}

// operador ternario

condition ? true : false;

ejemplo: 

var votacion = edad === 18 ? "Tienes 18 años" : "No tienes 18 años";

// if (true) {
//     console.log("hola");
// } else {
//     console.log("soy falso");
// }

// if (false) {
//     console.log("hola");
// } else if (true) {
//     console.log("algo salio mal")
// } else {
//     console.log("todo mal")
// }