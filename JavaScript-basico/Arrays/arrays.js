var frutas = ["Platano", "Cereza", "Fresa", "Manzana"];

console.log(frutas);

var añadirUltimo = frutas.push("Uva");
// al llamar a frutas tendrá el elemento Uva al final del array, var frutas = ["Platano", "Cereza", "Fresa", "Manzana", "Uva"];

var eliminarUltimo = frutas.pop("Uva");
// al llamar a frutas no tendrá el elemento Uva al final del array, var frutas = ["Platano", "Cereza", "Fresa", "Manzana"];

var añadirPrincipio = frutas.unshift("Uva");
// al llamar a frutas tendrá el elemento Uva al principio del array, var frutas = ["Uva", "Platano", "Cereza", "Fresa", "Manzana"];

var eliminarPrimero = frutas.shift("Uva");
// al llamar a frutas no tendrá el elemento Uva al principio del array, var frutas = ["Platano", "Cereza", "Fresa", "Manzana"];

var numeroPosicion = frutas.indexOf("Cereza")
// al llamar numeroPosicion nos retorna el numero de posicion en el array en este caso numeroPosicion = 1;